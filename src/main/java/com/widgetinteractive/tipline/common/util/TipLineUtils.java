/**
 *    This file is part of TipLine.
 *
 *    TipLine is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    TipLine is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with TipLine.
 *    If not, see <http://www.gnu.org/licenses/>.
 */

package com.widgetinteractive.tipline.common.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.widgetinteractive.tipline.common.ConfigHandler;
import com.widgetinteractive.tipline.common.DisplayHandler;
import com.widgetinteractive.tipline.common.EventHandler;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.text.TextFormatting;

public class TipLineUtils {

	public static void drawTips(FontRenderer fontRenderer) {
		Integer maxWidth = new ScaledResolution(Minecraft.getMinecraft()).getScaledWidth();
		Integer maxHeight = new ScaledResolution(Minecraft.getMinecraft()).getScaledHeight();
		Integer xOffset = 5, yOffset = 5, xTitleOffset = 5;
		Boolean centered = false, right = false;
		
		Integer tipIndex = EventHandler.tipIndex + 1;
		String tipTitle = ConfigHandler.tipTitleText.replace("<number>", tipIndex.toString());
		String colorTipTitles = DisplayHandler.getColor(ConfigHandler.colorTipTitles.toString());
		String colorTips = DisplayHandler.getColor(ConfigHandler.colorTips.toString());
		
		List<String> preFormattedTip = fontRenderer.listFormattedStringToWidth(EventHandler.tip, maxWidth - 10);
		List<String> formattedTip = preFormattedTip.stream().flatMap(s -> Arrays.stream(s.split("%n"))).collect(Collectors.toList());
		Integer tipHeight = formattedTip.size() * 10;
		
		switch (ConfigHandler.position) {
			case TOPLEFT :
				break;
			case TOPCENTER :
				xOffset = maxWidth / 2;
				xTitleOffset = maxWidth / 2;
				centered = true;
				break;
			case TOPRIGHT :
				xTitleOffset = maxWidth - 5 - fontRenderer.getStringWidth(tipTitle);
				right = true;
				break;
			case BOTTOMLEFT :
				yOffset = maxHeight - tipHeight - 15;
				break;
			case BOTTOMCENTER :
				xOffset = maxWidth / 2;
				xTitleOffset = maxWidth / 2;
				yOffset = maxHeight - tipHeight - 15;
				centered = true;
				break;
			case BOTTOMRIGHT :
				xTitleOffset = maxWidth - 5 - fontRenderer.getStringWidth(tipTitle);
				yOffset = maxHeight - tipHeight - 15;
				right = true;
				break;
		}
		
		
		
		if (yOffset == 5) {
			if (ConfigHandler.tipTitleDisplay) {
				if (centered == true) {
					Minecraft.getMinecraft().ingameGUI.drawCenteredString(fontRenderer, TextFormatting.BOLD.toString() + colorTipTitles + tipTitle, xTitleOffset, yOffset, 0xFFFFFF);
					yOffset += 10;
					
					for (String row : formattedTip) {
						Minecraft.getMinecraft().ingameGUI.drawCenteredString(fontRenderer, colorTips + row.trim(), xOffset, yOffset, 0xFFFFFF);
						yOffset += 10;
					}
				} else {
					Minecraft.getMinecraft().ingameGUI.drawString(fontRenderer, TextFormatting.BOLD.toString() + colorTipTitles + tipTitle, xTitleOffset, yOffset, 0xFFFFFF);
					yOffset += 10;
					
					for (String row : formattedTip) {
						if (right == true) {
							xOffset = maxWidth - 5 - fontRenderer.getStringWidth(row);
						}
						
						Minecraft.getMinecraft().ingameGUI.drawString(fontRenderer, colorTips + row.trim(), xOffset, yOffset, 0xFFFFFF);
						yOffset += 10;
					}
				}
			} else {
				if (centered == true) {
					for (String row : formattedTip) {
						Minecraft.getMinecraft().ingameGUI.drawCenteredString(fontRenderer, colorTips + row.trim(), xOffset, yOffset, 0xFFFFFF);
						yOffset += 10;
					}
				} else {
					for (String row : formattedTip) {
						if (right == true) {
							xOffset = maxWidth - 5 - fontRenderer.getStringWidth(row);
						}
						
						Minecraft.getMinecraft().ingameGUI.drawString(fontRenderer, colorTips + row.trim(), xOffset, yOffset, 0xFFFFFF);
						yOffset += 10;
					}
				}
			}
		} else {
			if (ConfigHandler.tipTitleDisplay) {
				if (centered == true) {
					if (formattedTip.size() > 1) {
						yOffset -= 10;
					}

					Minecraft.getMinecraft().ingameGUI.drawCenteredString(fontRenderer, TextFormatting.BOLD.toString() + colorTipTitles + tipTitle, xOffset, yOffset, 0xFFFFFF);					
					yOffset += 10;
					
					for (String row : formattedTip) {
						Minecraft.getMinecraft().ingameGUI.drawCenteredString(fontRenderer, colorTips + row.trim(), xOffset, yOffset, 0xFFFFFF);
						yOffset += 10;
					}
				} else {
					if (formattedTip.size() > 1) {
						yOffset -= 10;
					}
					
					Minecraft.getMinecraft().ingameGUI.drawString(fontRenderer, TextFormatting.BOLD.toString() + colorTipTitles + tipTitle, xTitleOffset, yOffset, 0xFFFFFF);
					yOffset += 10;

					for (String row : formattedTip) {
						if (right == true) {
							xOffset = maxWidth - 5 - fontRenderer.getStringWidth(row);
						}
						
						Minecraft.getMinecraft().ingameGUI.drawString(fontRenderer, colorTips + row.trim(), xOffset, yOffset, 0xFFFFFF);
						yOffset += 10;
					}
					
				}
			} else {
				if (centered == true) {
					for (String row : formattedTip) {
						Minecraft.getMinecraft().ingameGUI.drawCenteredString(fontRenderer, colorTips + row.trim(), xOffset, yOffset, 0xFFFFFF);
						yOffset += 10;
					}
				} else {
					for (String row : formattedTip) {
						if (right == true) {
							xOffset = maxWidth - 5 - fontRenderer.getStringWidth(row);
						}
						
						Minecraft.getMinecraft().ingameGUI.drawString(fontRenderer, colorTips + row.trim(), xOffset, yOffset, 0xFFFFFF);
						yOffset += 10;
					}
					
				}
			}
		}
	}
}