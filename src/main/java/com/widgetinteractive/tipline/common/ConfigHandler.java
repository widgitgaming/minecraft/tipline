/**
 *    This file is part of TipLine.
 *
 *    TipLine is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of
 *    the License, or (at your option) any later version.
 *
 *    TipLine is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with TipLine.
 *    If not, see <http://www.gnu.org/licenses/>.
 */

package com.widgetinteractive.tipline.common;

import com.widgetinteractive.tipline.TipLine;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.common.config.Config.*;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Config(modid = TipLine.MODID)
public class ConfigHandler {

	@Name("Tips")
	@Comment("Set the tips to display.")
	public static String[] _tipList = new String[] {
			"You can set as many tips as you want!"
	};
	
	@Name("Color: Tips")
	@Comment("Set the color for tips.")
	public static DisplayHandler.Color colorTips = DisplayHandler.Color.WHITE;
	
	@Name("Color: Tip Titles")
	@Comment("Set the color for tip titles.")
	public static DisplayHandler.Color colorTipTitles = DisplayHandler.Color.YELLOW;
	
	@Name("Position")
	@Comment("Set where the tips should be displayed.")
	public static DisplayHandler.Position position = DisplayHandler.Position.BOTTOMLEFT;
	
	@Name("Display Tip Titles")
	@Comment("Set whether or not to display the title line.")
	public static boolean tipTitleDisplay = true;
	
	@Name("Title Text")
	@Comment("Set the title text. Use <number> for the tip number.")
	public static String tipTitleText = "Tip #<number>:";

	@Mod.EventBusSubscriber(modid = TipLine.MODID)
	private static class ConfigEventHandler {
		
		@SubscribeEvent
		public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
			if (event.getModID().equals(TipLine.MODID)) {
				ConfigManager.sync(TipLine.MODID, Config.Type.INSTANCE);
			}
		}
	}
}